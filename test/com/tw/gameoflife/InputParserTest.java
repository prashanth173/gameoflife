package com.tw.gameoflife;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static com.tw.gameoflife.Cell.alive;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

public class InputParserTest {

	@Test
	public void testForTransformationForSingleInputString() {
		InputParser parser = new InputParser();
		Set<Cell> actual = parser.transform(singletonList("1,1"));
		Set<Cell> expected = new HashSet<>(singletonList(alive(1, 1)));
		assertEquals(actual, expected);
	}

	@Test
	public void testForTransformationForMultipleInputStrings() {
		InputParser parser = new InputParser();
		Set<Cell> actual = parser.transform(asList("1,1", "1,2", "2,1", "2,2"));
		Set<Cell> expected = new HashSet<>(asList(alive(1, 1), alive(1, 2), alive(2, 1), alive(2, 2)));
		assertEquals(actual, expected);
	}
}
