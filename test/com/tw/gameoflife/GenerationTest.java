package com.tw.gameoflife;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static com.tw.gameoflife.Cell.alive;
import static org.junit.Assert.assertEquals;

public class GenerationTest {

	@Test
	public void testForBlockPattern() {
		Set<Cell> aliveCells = new HashSet<>();
		aliveCells.add(alive(1, 1));
		aliveCells.add(alive(1, 2));
		aliveCells.add(alive(2, 1));
		aliveCells.add(alive(2, 2));
		Generation generation = new Generation(aliveCells);
		assertEquals(generation, generation.transform());
	}

	@Test
	public void testForBoatPatter() {
		Set<Cell> aliveCells = new HashSet<>();
		aliveCells.add(alive(0, 1));
		aliveCells.add(alive(1, 0));
		aliveCells.add(alive(2, 1));
		aliveCells.add(alive(0, 2));
		aliveCells.add(alive(1, 2));
		Generation generation = new Generation(aliveCells);
		assertEquals(generation, generation.transform());
	}


	@Test
	public void testForBlinkPattern() {
		Set<Cell> aliveCells = new HashSet<>();
		aliveCells.add(alive(1, 1));
		aliveCells.add(alive(1, 0));
		aliveCells.add(alive(1, 2));
		Generation generation = new Generation(aliveCells);
		Set<Cell> expectedAlive = new HashSet<>();
		expectedAlive.add(alive(1, 1));
		expectedAlive.add(alive(0, 1));
		expectedAlive.add(alive(2, 1));
		Generation expected = new Generation(expectedAlive);
		assertEquals(expected, generation.transform());
	}

	@Test
	public void testForToadPattern() {
			Set<Cell> aliveCells = new HashSet<>();
			aliveCells.add(alive(1, 1));
			aliveCells.add(alive(1, 2));
			aliveCells.add(alive(1, 3));
			aliveCells.add(alive(2, 2));
			aliveCells.add(alive(2, 3));
			aliveCells.add(alive(2, 4));
			Generation generation = new Generation(aliveCells);
			Set<Cell> expectedAlive = new HashSet<>();
			expectedAlive.add(alive(0, 2));
			expectedAlive.add(alive(1, 1));
			expectedAlive.add(alive(1, 4));
			expectedAlive.add(alive(2, 1));
			expectedAlive.add(alive(2, 4));
			expectedAlive.add(alive(3, 3));
			Generation expected = new Generation(expectedAlive);
			assertEquals(expected, generation.transform());
	}

}
