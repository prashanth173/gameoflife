package com.tw.gameoflife;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CoordinateTest {

	@Test
	public void testForAllNeighbouringCoordinates() {
		Coordinate oneAndOne = new Coordinate(1, 1);
		Set<Coordinate> expected = new HashSet<>();
		expected.add(new Coordinate(0, 0));
		expected.add(new Coordinate(0, 1));
		expected.add(new Coordinate(0, 2));
		expected.add(new Coordinate(1, 0));
		expected.add(new Coordinate(1, 2));
		expected.add(new Coordinate(2, 0));
		expected.add(new Coordinate(2, 1));
		expected.add(new Coordinate(2, 2));
		Set<Coordinate> actual = oneAndOne.getNeighbours();
		assertEquals(expected, actual);
	}

	@Test
	public void hashCodeOfOneOneAndOneOneIsEqual() {
		Coordinate oneAndOne = new Coordinate(1, 1);
		Coordinate oneOneSecond = new Coordinate(1, 1);
		assertEquals(oneAndOne.hashCode(), oneOneSecond.hashCode());
	}

	@Test
	public void hashCodeOfCoordinateOneThirtyOneAndTwoZeroIsNotEqual() {
		Coordinate oneAndThirtyOne = new Coordinate(1, 31);
		Coordinate twoAndZero = new Coordinate(2, 0);
		assertNotEquals(oneAndThirtyOne.hashCode(), twoAndZero.hashCode());
	}
}
