package com.tw.gameoflife;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static com.tw.gameoflife.Cell.*;
import static org.junit.Assert.assertEquals;

public class CellTest {

	@Test
	public void testForNeighboursOfCellWithCoordinateOneAndOne() {
		Set<Cell> expected = new HashSet<>();
		expected.add(dead(0, 0));
		expected.add(dead(0, 1));
		expected.add(dead(0, 2));
		expected.add(dead(1, 0));
		expected.add(alive(1, 2));
		expected.add(dead(2, 0));
		expected.add(alive(2, 1));
		expected.add(alive(2, 2));
		Set<Cell> aliveList = new HashSet<>();
		aliveList.add(alive(1, 1));
		aliveList.add(alive(1, 2));
		aliveList.add(alive(2, 1));
		aliveList.add(alive(2, 2));
		Set<Cell> actual = alive(1, 1).generateListOfNeighbourCells(aliveList);
		assertEquals(expected, actual);
	}


	@Test
	public void testForIsAliveReturnsTrueForCellWhichIsAlive() {
		Cell liveCell = alive(1, 1);
		assertEquals(true, liveCell.isAlive());
	}

	@Test
	public void testForIsAliveReturnsFalseForCellWhichIsDead() {
		Cell liveCell = dead(1, 1);
		assertEquals(false, liveCell.isAlive());
	}

	@Test
	public void testForADeadCellWithTwoNeighboursWillRemainDead() {
		assertEquals(dead(1, 1), dead(1, 1).getTransformedCell(2));
	}

	@Test
	public void testForACellWithLessThanTwoNeighboursWillDie() {
		Cell liveCell = alive(1, 1);
		Cell actual = liveCell.getTransformedCell(1);
		Cell expected = dead(1, 1);
		assertEquals(expected, actual);
	}

	@Test
	public void testForAnyCellWithMoreThanThreeAliveNeighboursWillDie() {
		Cell liveCell = alive(1, 1);
		Cell actual = liveCell.getTransformedCell(4);
		Cell expected = dead(1, 1);
		assertEquals(expected, actual);
	}

	@Test
	public void testForAnyLiveCellWithTwoLiveNeighbourWillRemainAlive() {
		Cell liveCell = alive(1, 1);
		Cell actual = liveCell.getTransformedCell(2);
		Cell expected = alive(1, 1);
		assertEquals(expected, actual);
	}

	@Test
	public void testForAnyLiveCellWithThreeLiveNeighbourWillRemainAlive() {
		Cell liveCell = alive(1, 1);
		Cell actual = liveCell.getTransformedCell(3);
		Cell expected = alive(1, 1);
		assertEquals(expected, actual);
	}

	@Test
	public void testForDeadCellWithThreeLiveNeighboursWillBecomeAlive() {
		Cell liveCell = dead(1, 1);
		Cell actual = liveCell.getTransformedCell(3);
		Cell expected = alive(1, 1);
		assertEquals(expected, actual);
	}
}
