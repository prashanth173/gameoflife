package com.tw.gameoflife;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

//Understands single unit of life
class Cell {
	private final Coordinate cellPosition;
	private final CellState cellState;

	private Cell(Coordinate cellPosition, CellState cellState) {
		this.cellPosition = cellPosition;
		this.cellState = cellState;
	}

	static Cell alive(int xCoordinate, int yCoordinate) {
		return new Cell(new Coordinate(xCoordinate, yCoordinate), CellState.ALIVE);
	}

	static Cell dead(int xCoordinate, int yCoordinate) {
		return new Cell(new Coordinate(xCoordinate, yCoordinate), CellState.DEAD);
	}

	static Cell alive(Coordinate coordinate) {
		return new Cell(coordinate, CellState.ALIVE);
	}

	static Cell dead(Coordinate coordinate) {
		return new Cell(coordinate, CellState.DEAD);
	}


	boolean isAlive() {
		return cellState.equals(CellState.ALIVE);
	}

	Set<Cell> generateListOfNeighbourCells(Set<Cell> aliveCells) {
		Set<Coordinate> neighbourCoordinates = cellPosition.getNeighbours();
		Set<Cell> allNeighbourCells = new HashSet<>();
		allNeighbourCells.addAll(getDeadNeighbours(aliveCells, neighbourCoordinates));
		allNeighbourCells.addAll(getAliveNeighbours(aliveCells, neighbourCoordinates));
		return allNeighbourCells;
	}

	Cell getTransformedCell(int liveNeighbours) {
		if(hasLessThan2OrMoreThan3Neighbours(liveNeighbours) || isDeadAndHasLessThan3LiveNeighbours(liveNeighbours)) {
			return new Cell(cellPosition, CellState.DEAD);
		}
		return createLiveCell(this);
	}

	private boolean isDeadAndHasLessThan3LiveNeighbours(int liveNeighbours) {
		return !isAlive() && liveNeighbours < 3;
	}

	private boolean hasLessThan2OrMoreThan3Neighbours(int liveNeighbours) {
		return liveNeighbours < 2 || liveNeighbours > 3;
	}

	private Set<Cell> getAliveNeighbours(Set<Cell> aliveCells, Set<Coordinate> neighbourCoordinates) {
		return aliveCells.stream()
				.filter(cell -> neighbourCoordinates.contains(cell.cellPosition))
				.collect(toSet());
	}

	private Set<Cell> getDeadNeighbours(Set<Cell> aliveCells, Set<Coordinate> neighbourCoordinates) {
		Set<Coordinate> aliveCellsCoordinates = aliveCells.stream()
				.map(x -> x.cellPosition)
				.collect(toSet());

		return neighbourCoordinates.stream()
				.filter(coordinate -> !aliveCellsCoordinates.contains(coordinate))
				.map(Cell::dead)
				.collect(toSet());
	}

	private Cell createLiveCell(Cell deadCell) {
		return new Cell(deadCell.cellPosition, CellState.ALIVE);
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) return true;
		if (other == null || getClass() != other.getClass()) return false;

		Cell otherCell = (Cell) other;

		return cellPosition.equals(otherCell.cellPosition) && cellState == otherCell.cellState;
	}

	@Override
	public int hashCode() {
		return cellPosition.hashCode();
	}

	@Override
	public String toString() {
		return cellPosition.toString();
	}
}
