package com.tw.gameoflife;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

//Understands the evolution of Universe
class GameOfLife {

	public static void main(String arg[]) throws IOException {
		InputParser parser = new InputParser();
		Set<Cell> liveCells = parser.transform(getGameInput());
		Generation generation = new Generation(liveCells);
		generation = generation.transform();
		System.out.println("Transformed Grid");
		generation.display();
	}

	private static List<String> getGameInput() throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		List<String> lines = new ArrayList<>();
		String line;
		while (!(line = bufferedReader.readLine()).equals("")) {
			lines.add(line.trim());
		}
		return lines;
	}

}

