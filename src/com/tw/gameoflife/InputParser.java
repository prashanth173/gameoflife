package com.tw.gameoflife;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.tw.gameoflife.Cell.alive;

// Understands to transform List of input Strings to Set of Live cells
class InputParser {

	Set<Cell> transform(List<String> inputLines) {
		Set<Cell> liveCells = inputLines.stream()
				.map(this::obtainCell)
				.collect(Collectors.toSet());
		return liveCells;
	}

	private Cell obtainCell(String line) {
		String[] xAndy = line.trim().split(",");
		int x = Integer.parseInt(xAndy[0].trim());
		int y = Integer.parseInt(xAndy[1].trim());
		return alive(x, y);
	}

}
