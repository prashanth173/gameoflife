package com.tw.gameoflife;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toConcurrentMap;
import static java.util.stream.Collectors.toSet;

//Understands the current state of life
class Generation {

	private Set<Cell> aliveCells;

	Generation(Set<Cell> aliveCells) {
		this.aliveCells = aliveCells;
	}

	Generation transform() {
		Map<Cell, Integer> allNeighbourCells = aliveCells.stream()
				.flatMap(this::streamOfNeighbourCells)
				.collect(toConcurrentMap(key -> key, value -> 1, Integer::sum));

		aliveCells = allNeighbourCells.entrySet().stream()
				.map(this::transformedCell)
				.filter(Cell::isAlive)
				.collect(toSet());
		return new Generation(new HashSet<>(aliveCells));
	}

	private Stream<Cell> streamOfNeighbourCells(Cell cell) {
		return cell.generateListOfNeighbourCells(aliveCells).stream();
	}

	private Cell transformedCell(Map.Entry<Cell, Integer> keyVal) {
		Cell cell = keyVal.getKey();
		int countOfLiveNeighbours = keyVal.getValue();
		return cell.getTransformedCell(countOfLiveNeighbours);
	}

	void display() {
		aliveCells.forEach(System.out::println);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Generation generation = (Generation) o;

		return aliveCells != null ? aliveCells.equals(generation.aliveCells) : generation.aliveCells == null;

	}

	@Override
	public int hashCode() {
		return aliveCells != null ? aliveCells.hashCode() : 0;
	}

	@Override
	public String toString() {
		return "generation{" +
				"aliveCells=" + aliveCells +
				'}';
	}
}
