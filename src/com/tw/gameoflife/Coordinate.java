package com.tw.gameoflife;

import java.util.HashSet;
import java.util.Set;

// Understands position of a cell in the infinite Grid
class Coordinate {
	private final int x;
	private final int y;

	Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}

	Set<Coordinate> getNeighbours() {
		Set<Coordinate> neighbourCoordinates = new HashSet<>();
		for (int i = x - 1; i <= x + 1; i++) {
			for (int j = y - 1; j <= y + 1; j++) {
				if (i != x || j != y) {
					neighbourCoordinates.add(new Coordinate(i, j));
				}
			}
		}
		return neighbourCoordinates;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Coordinate that = (Coordinate) o;

		return x == that.x && y == that.y;
	}

	@Override
	public int hashCode() {
		int result = 1;
		result = 31 * result + x * 191;
		result = 31 * result + y * 193;
		return result;
	}

	@Override
	public String toString() {
		return x + ", " + y;
	}

}
